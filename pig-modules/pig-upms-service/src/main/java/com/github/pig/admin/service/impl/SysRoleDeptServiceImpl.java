package com.github.pig.admin.service.impl;

import com.github.pig.admin.model.entity.SysRoleDept;
import com.github.pig.admin.mapper.SysRoleDeptMapper;
import com.github.pig.admin.service.SysRoleDeptService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色与部门对应关系 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-01-20
 */
@Service
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptMapper, SysRoleDept> implements SysRoleDeptService {
	
}
